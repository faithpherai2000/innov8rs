# 1. Project Title: TRIBE

< Explain what your project does and why >

Our project provides the resort or bussines with a platform of add-ons that include a dashboard with the view of visitor density and real-time location tracking. 
And the visitor gets an mobile app with an interactive map.

## Overview & Features

< features and special functions of your system>

Our platform has the ability for add-ons like:
- Book
- Pay
- Free/busy
- Activity calender

One of the most important things that any IOT project requires is a database to store the values, results and do some computation on them.
For our project we are using google spreadsheet to store and collect data to build the interactive mobile app on Android and Apple devices. 
The environment that we are using is glide.

## Demo / Proof of work

< Fotos, video of your working project as proof of work>

## Deliverables

< The deliverables of the project, so we know when it is complete>

*Interactive map*

![map](docs/img/tr/map+.jpg)

*Mobile app*

![start](docs/img/tr/start.jpg)


- Product pitch deck

[TRIBE pitch deck](https://docs.google.com/presentation/d/1H-FIwNTkSwomOy7MMpMZessw0HpeiyWvzj6HpUgi5fA/edit?usp=sharing)


## Project landing page/website (optional)

< if you created website or the like for your project link it up here>

Our project can be visited with a link:[TRIBE](https://tribe-innov8rs.glideapp.io/) or with a QRcode

![qrcode](docs/img/tr/qrcode.jpg)

## the team & contact

< Time to shine with your team .. show their expertise and contribution to the project >

## The INNOV8RS

![inno](docs/img/tr/inno.png)

![This is us](docs/img/tr/us.jpeg)

### Ivy Amatredjo

https://codettes-boothcamp.gitlab.io/students/ivy-amatredjo


### Shofia Notoesowito

https://codettes-boothcamp.gitlab.io/students/shofia-notosoewito

### Faith Pherai

https://codettes-boothcamp.gitlab.io/students/faith-pherai

While making TRIBE we did everything together. From troubleshooting to figuring out how to make things work.
When one came with an idea we all stepped in and gave a bit of our own knowledge to help.
We are a GREAT TEAM after all!!

